# Deepin en Español 3.0
En desarrollo...

## Arreglos
* Diseño compactado, reducido los márgenes en los diagonales y las publicaciones
* Maximizado el límite de ancho a 1500px, para pantallas con 2048px de ancho
* Espacio de altura optimizado para pantallas 2K
* Cambiado `actividads` a `actividades`
