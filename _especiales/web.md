---
name:  "Términos de uso de la plataforma"
subtitle: Información sobre la plataforma Deepin en Español
image_path: /images/actividades/estudio.jpg
---

***Publicado el 26 de febrero de 2018 y efectivo desde la misma fecha. Es posible que sufran algunos cambios para casos específicos y avisaremos en la sección "Noticias".***

## Contexto

La página de la política del sitio web es un complemento de los términos de uso de la Comunidad Deepin en Español.

La PLATAFORMA

Al estar desarrollado para los usuarios, usamos cookies para gestionar el uso de los mismso. Al visitar estas páginas, eres consciente que las cookies sean usadas según describamos en la sección correspondiente.

## Reglamento

Los administradores del proyecto son responsables de clarificar los estándares de comportamiento aceptable y se espera que tomen medidas correctivas y apropiadas en respuesta a situaciones de conducta inaceptable.

Los administradores del proyecto tienen el derecho y la responsabilidad de eliminar, editar o rechazar APORTES que no estén alineadas con este Código de Conducta y aplicarán las sanciones en la sección Aplicación.

<h3 id="responsabilidaddelosaportesdelaweb">Responsabilidad de los aportes de la web</h3>

En caso que quieras aportar alguna publicación los administradores velarán que:

<ul>

<li>No deben ser plagiadas de páginas web. En caso de citar, se necesita mencionar la fuente si fuera posible.</li>

<li>No deben promover la publicidad no deseada (SPAM) o pornografía infantil.</li>

<li>Al adjuntar y/o enlazar, deben estar libres de virus o software malintencionado.</li>

<li>Las publicaciones pagadas por una empresa deben ser señaladas al final de la página, sustentando el motivo.</li>
</ul>

<h3 id="responsabilidaddelosderechosdeautor">Responsabilidad de los derechos de autor</h3>

Las redacciones de solo texto son licenciadas por defecto bajo <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike</a> y <a href="https://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License</a>.

Para más detalles, adjuntamos <a href="{{ site.url }}/licencia">el archivo "Licencia"</a>. Puedes solicitar una licencia más permisiva que la primera. Por ejemplo, para distribuir sin copyleft recomendamos <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution</a>.

Otro contenido es propiedad intelectual de sus respectivos autores. Mayormente, etiquetamos "fair use" u "uso legítimo" con la intención de no lucrar hasta lo permitido por las leyes.

## Uso de los repositorios de Deepin

Para la publicación del código en servicios Git (como Github o Gitlab). Eres consciente de conocer el funcionamiento como <em>commits</em>, código, ediciones de documentación, issues, etcétera.

## Privacidad

Los administradores usarán la página web para:

* Comprobar si el usuario está usando correctamente este sitio web.
* Evitar la duplicación de usuarios para incentivar popularidad.
* Comprobar la dirección IP si forma parte de la lista de solicitudes no deseadas (usando Wordefence).
