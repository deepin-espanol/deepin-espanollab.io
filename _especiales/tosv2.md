---
name:  "Términos de uso"
subtitle: Información de la comunidad (versión actual)
image_path: /images/actividades/estudio.jpg
---

***Publicado el 26 de febrero de 2018 y efectivo desde la misma fecha. Es posible que sufran algunos cambios para casos específicos y avisaremos en la sección "Noticias".***

Bienvenidos a Deepin en Español y gracias por ser parte de ésta. Antes de continuar, hemos elaborado los términos de uso para los integrantes. La comunidad está integrada usuarios, capacitados y administradores comprometidos a establecer un AMBIENTE integral, segura y que ofrece una la experiencia transpartente y globalmente posible.

Recuerda que para realizar APORTES (el material, el contenido, los artículos, etcétera) y responsabilidades debes tener al menos 18 años de edad (o su equivalente acorde a las leyes del país que resides). En caso que seas menor de edad, necesitarás el permiso de un familiar o tutor para hacerse cargo de las acciones.

<h3 id="alcance">Alcance</h3>

Nuestro objetivo es hacer que los usuarios puedan participar y cooperar en **AMBIENTES que hemos elaborado** bajo representación del proyecto o comunidad. Incluye la página web, redes sociales, eventos tanto internos como especiales o presentaciones con personas designadas en eventos <em>online</em> u <em>offline</em>. El contexto de ese AMBIENTE puede ser aclarado de forma directa por los administradores del proyecto.

<h2 id="cdigodeconducta">Código de conducta</h2>

Nuestra comunidad debe seguir las reglas para evitar situaciones que dañen nuestra imagen:

<ul>
<li>Opina sin miedo pero con respeto: Evita los insultos, conversaciones hostiles o críticas destructivas. No apoderes ideas ajenas para propósitos indebidos.</li>

<li>Si muestras antipático serás antipático: Si la COMUNIDAD ve al participante como enemigo, será tratado como tal. De forma similar, si <strong>acosas</strong>, aplicas <em>trolling</em> o <strong>distribuyes pornografía</strong>.</li>

<li>Aporta con humildad: la COMUNIDAD hace aportes para mejorar. Si quieres que la COMUNIDAD cumpla las necesidades, mejóralas.</li>

<li>Participa si vale la pena: No estás obligado en realizar actividades por varios motivos. De lo contrario, respeta las decisiones que toman los administradores en determinados eventos.</li>

<li>Ante todo educación: Para que el AMBIENTE sea lo más educado posible, trata de hacerlo con valores humanos. Incluso la COMUNIDAD puede ser divertida, social o amigable, sin faltar el respeto de otros.</li>

<li>Mantén el perfil profesional: Nuestra intención es que Deepin tenga relevancia en este AMBIENTE. Puedes ofrecer colaboración para Deepin, la empresa Deepin Wuhan y proyectos que sustente esta COMUNIDAD. Casos fuera de tópico deben evitarse.</li>

</ul>

<h2 id="aplicacin">Aplicación</h2>

Ejemplos de abuso, acoso, falisificación de indentidades, u otro tipo de comportamiento inaceptable puede ser reportado al equipo del proyecto en <a href="{{ site.url }}/contact/">la página Contacto</a>. Todas las quejas serán revisadas e investigadas, generando un resultado apropiado a las circunstancias. El equipo del proyecto está obligado a mantener confidencialidad de la persona que reportó el incidente. Detalles específicos acerca de las políticas de aplicación pueden ser publicadas por separado.

Administradores que no sigan o que no hagan cumplir este Código de Conducta pueden ser eliminados de forma temporal o permanente del equipo administrador. De forma similar sucede con los usuarios en general.

En caso que el usuario promueva el terrorismo o pornografía infantil, amenace a los usuarios y tenga antecedentes, será reportado en privado a las autoridades correspondientes. Algunas sugerencias en <a href="https://www.genbeta.com/a-fondo/como-denunciar-a-la-policia-si-encuentro-contenido-pedofilo-en-internet">esta página</a>.

<h3 id="limitacioneslegales">Limitaciones legales</h3>

El contenido desarrollado por la comunidad está bajo una administración de responsabilidad limitada. Dicha administración no tiene la obligación de participar frente a:

<ul>
<li>Las actividades con fines ajenos a la organización, incluyendo las acciones malintencionadas.</li>

<li>Pérdidas materiales o económicas realizadas por la comunidad sin ser probadas ni revisadas con antelación; o</li>

<li>Las acciones que podrían incitar a las actividades ilegales. En caso que ocurra, la comunidad no estará involucrada y cualquier pérdida del demandante correrá directamente al individuo responsables.</li>
</ul>

<h2 id="agradecimientos">Agradecimientos</h2>

Este Código de Conducta es una adaptación del <a href="http://contributor-covenant.org">Contributor Covenant</a>, versión 1.4, disponible en <a href="http://contributor-covenant.org/version/1/4/es/">http://contributor-covenant.org/version/1/4/es/</a>

Contiene extractos de <a href="https://ondahostil.wordpress.com/politica-de-troleo/">OndaHostil</a>, licenciado bajo CC-BY-SA 4.0.

Puedes crear una nueva versión de los términos, siempre y cuando nos atribuyas a Deepin en Español.
