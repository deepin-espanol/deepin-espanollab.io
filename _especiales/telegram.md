---
name:  "Bienvenida de Telegram"
subtitle: Saludos e información de la comunidad
image_path: /images/actividades/estudio.jpg
---

***Publicado el 26 de febrero de 2018 y efectivo desde la misma fecha. Es posible que sufran algunos cambios para casos específicos y avisaremos en la sección "Noticias".***

Bienvenidos y bienvenidas al grupo <a href="{{ site.url }}{{ site.baseurl }}/about/">Deepin en Español</a> en la plataforma Telegram.

Gracias por entrar al grupo. Es un gusto que formes parte de nuestra comunidad que busca ser amigable, colaborativo y seguro. Buscamos nuevos compañeros(as) educados(as), respetuosos(as) y con buena vibra social, para ello tendrás seguir algunos puntos claves.

Recuerda, Deepin en Español no solo se conforma por nuestro grupo de Telegram, también podrás encontrar nuestra presencia en otros medios. Lo explicaremos al final.

### De qué hablamos en Deepin en Español
En este grupo debatimos, desarrollamos recursos y ofrcemos apoyo a los usuarios de Deepin. También hablamos temas relacionados como Debian, GNU/Linux, Software Libre y <a href="{{ site.data.externalurl.ayudadeepin }}/deepin/dudas/">más allá</a>. Además, realizamos <a href="{{ site.url }}{{ site.baseurl }}/actividades/">actividades</a> para fortalecer la comunidad.


Recuerda que si quieres tener una buena comunicación con los integrantes, eres bienvenido(a) a darnos sugerencias. En ocasiones, estrenaremos grupos y sesiones especiales para discutir otros temas por anunciar.
Además, a medida que aportemos, los tutoriales lo añadimos a la página <a href="{{ site.data.externalurl.ayudadeepin }}">Ayuda de Deepin</a> para que sean consultados en el futuro (relacionado a este sistema operativo).

### Cómo trabajamos
En este grupo:
* Reportamos fallos en Deepin; por favor, describe los detalles (si es un bug del sistema) o señala la información del equipo (si es problema de conectividad, la gráfica, etcétera).
* Buscamos talento de desarrolladores de software, traductores, diseñadores gráficos o colaboradores relacionados a la COMUNIDAD. Si tienes la posibilidad de participar, dependiendo del tiempo, anuncia en este grupo.
* Anunciamos novedades junto al personal oficial de Deepin. Para eso, nos comunicamos en inglés con el equipo de Deepin Wuhan cuando sea necesario.
* Nos enfocamos solo a Deepin, especialmente en el software. Si quieres portar sofware o <a href="{{ site.url }}{{ site.baseurl }}/basedata/">el entorno de escritorio</a>, eres bienvenido. Sin embargo, y para no desvíar la meta, no ofreceremos soporte a bases ajenas a Deepin (o a Debian, que está basada el sistema operativo).

Las siguientes acciones solo pueden ser controladas por los administradores:
* Ten cuidado con hacer uso inadecuado de bots. Usamos bots como Combot para realizar limpieza o crear mensajes de bienvenida, no usamos otros bots para evitar sobrecargar al grupo. Tampoco uses los comandos como juego (por ejemplo, tocar icono de <code>/</code> en el cuadro de diálogo y jugar). Esas órdenes de los bots se usan en situaciones concretas. Si hubo un error, borra los mensajes a tiempo.
* Off-Topic: Este grupo se enfoca en Deepin y temas afines ya explicados. En lo posible, evita enfocarte a temas ajenos muy polémicos o que desvirtúan el verdadero objetivo de la comunidad.
* Software experimental: Si el programa que vas a portar no es para uso cotidiano, y que podría ser motivo de ***romper el sistema operativo***, advierte al público (con una guía de instalación y una etiqueta tipo <code>#test</code> si es posible).

Estas son las acciones que se deben tomar en cuenta para que el AMBIENTE sea de respeto y transparencia:
* No escribas más de lo debido: Envíar varias veces el mismo mensaje por un breve tiempo no es buena idea, escribir "asdfgh" tampoco, o enviar demasiados stickers y GIFS <em>sin valor expresivo arruina el flujo de comunicación</em> (en inglés sería ***flood***, inundación).
* Comparte con cuidado: Promocionar contenido con fines lucrativos o intrusivos es inaceptable. Pide permiso a los administradores si quieres anunciar una noticia relevante en este grupo.
* No emplees el acoso: Molestar a la comunidad o a sus administradores. Si no tienes confianza con los usuarios, actúa con dignidad y evita actitudes sospechosas. Pero si tienes amistad o confianza, mantén el respeto para dar el ejemplo.
* No provoques: El contenido chocante como el material pornográfico, violencia gratuita o contenido que incite al daño de la dignidad humana es motivo de suspensión o explusión, dependiendo de la intensidad.
* Respeta los protocolos de seguridad: No suplantes cuentas, no difundas contenido sin confirmación oficial o aproveches la confianza para dañar la imagen de la organización.

### Finalización

En caso que hayas violado las reglas. Nuestro equipo reaccionará de varias formas:

* Borrará los mensajes que desvirtúen el AMBIENTE de Deepin como temas fuera de tópico, contenido inadecuado o similares.
* Serás <b>advertido(a)</b> por primera vez si has cometido por equivocación y que no atente a la comunidad.
* Al cometer actividades que genere toxicidad al AMBIENTE, serías <em>suspendido(a) temporalmente</em> del grupo. De lo contrario, si no se puede solucionar el problema, serás <b>expulsado(a) permanentemente</b>.

### Anexo
* Para quienes no conocen el sistema operativo, tenemos lista <a href="{{ site.url }}{{ site.baseurl }}/actividades">Actividades</a> y un catálogo de <a href="{{ site.data.externalurl.ayudadeepin }}/deepin/dudas">dudas frecuentes</a> para ganar tiempo.
* Si quieres descargar visita <a href="{{ site.data.externalurl.ayudadeepin }}/deepin/download">la página dedicada</a>, con los espejos según el país que te encuentres.
* Estamos también en <a href="https://plus.google.com/communities/115544729561220868525">Google+</a> (administrado por @Car), <a href="https://github.com/deepin-espanol">Github</a> (creado por Diego, administrado junto a @Ampiflow02 y @G4SP3R) y <a href="https://www.facebook.com/DeepinOSenEspanol/">Facebook</a> (administrado por Roberto Bervih).
* Para mantener el control del grupo, tenemos a Combot y un <a href="https://combot.org/chat/-1001050493375">registro de actividades</a>, incluyendo las horas con mayor intercambio de mensajes.
* Estas reglas no aplican al blog de Deepin en Español, <a href="{{ site.url }}{{ site.baseurl }}/tosv2">usamos los terminos de uso en general</a>.
