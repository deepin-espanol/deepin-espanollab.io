---
name:  "Agradecimientos"
subtitle: Agradecimientos especiales
image_path: /images/actividades/estudio.jpg
---

Damos las gracias a personas y organizaciones:

<ul>
<li>CloudCanon por las plantillas Base y Urban para el diseño del sitio web.</li>
<li>Alex Ávalo y la empresa OVH.com por el dominio "deepin.ovh".</li>
<li>Diego Sanguinetti por desarrollar el blog y la Ayuda de Deepin.</li>
<li>Alejandro Camarena, Alvaro Samudio y Carlos Moreno por el diseño de los iconos.</li>
<li>Los que participan en nuestro <a href="https://t.me/deepinenespanol">grupo de Telegram</a>.</li>
<li><a href="https://pixabay.com">Pixabay</a> por su catálogo de fotos bajo Creative Commons Zero.</li>
</ul>

Los aportes son voluntarios, sin ellos el proyecto no se realizaría como debería.

También agradecemos a los colabores que se encargan de proveer herramientas para Deepin. Puedes revisar en <a href="https://drive.google.com/drive/folders/0B5CDMGpiU7lWZ3hHXzY4bUMzLUk">Deepin Mirror Plus</a>.
