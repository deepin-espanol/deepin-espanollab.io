---
name:  "Términos de uso (agradecimientos)"
subtitle: Información de la comunidad (versión original)
image_path: /images/actividades/estudio.jpg
---

**Publicado el 30 de septiembre de 2017 y actualizado el 26 de febrero de 2017. Es posible que sufran algunos cambios para casos específicos y avisaremos en la sección "Noticias".**

En el interés de fomentar una comunidad transparente y acogedora, nosotros como contribuyentes y administradores de la COMUNIDAD (Deepin en Español) nos comprometemos a hacer proyectos de calidad con una colaboración inclusiva e integridada.

Antes de participar en el AMBIENTE (integrantes) y los APORTES (el material, el contenido, los artículos, etcétera) debes tener al menos 18 años de edad (o su equivalente acorde a las leyes del país que resides). En caso que seas menor de edad, necesitarás el permiso de un familiar o tutor para hacerse cargo de las acciones.

La página de la COMUNIDAD usa cookies, al visitar estas páginas, eres consciente que las cookies sean usadas según describamos en la sección correspondiente.

<h2 id="cdigodeconducta">Código de conducta</h2>

Cualquier aporte es bienvenido con reglas de conducta:

<ul>
<li>Opina sin miedo pero con respeto: Evita los insultos, conversaciones hostiles o críticas destructivas. No apoderes ideas ajenas para propósitos indebidos.</li>

<li>Si muestras antipático serás antipático: Si la COMUNIDAD ve al participante como enemigo, será tratado como tal. De forma similar, si <strong>acosas</strong>, aplicas <em>trolling</em> o <strong>distribuyes pornografía</strong>.</li>

**Aporta pero no exijas: la COMUNIDAD hace aportes para mejorar. Si quieres que la COMUNIDAD cumpla las necesidades, mejóralas.</li>

<li>Participa si vale la pena: No estás obligado en realizar actividades por varios motivos. De lo contrario, respeta las decisiones que toman los administradores en determinados eventos.</li>

<li>Ante todo educación: Para que el AMBIENTE sea lo más educado posible, trata de hacerlo con valores humanos. Incluso la COMUNIDAD puede ser divertida, social o amigable, sin pasar límites del entorno profesional.</li>
</ul>

<h2 id="organizacindelsitioweb">Organización del sitio web</h2>

Los administradores del proyecto son responsables de clarificar los estándares de comportamiento aceptable y se espera que tomen medidas correctivas y apropiadas en respuesta a situaciones de conducta inaceptable.

Los administradores del proyecto tienen el derecho y la responsabilidad de eliminar, editar o rechazar APORTES que no estén alineadas con este Código de Conducta y aplicarán las sanciones en la sección Aplicación.

<h3 id="responsabilidaddelosaportesdelaweb">Responsabilidad de los aportes de la web</h3>

En caso que quieras aportar alguna publicación los administradores velarán que:

<ul>
<li>Eres consciente de conocer el funcionamiento como <em>commits</em>, código, ediciones de documentación, <em>issues</em></li>

<li>No deben ser plagiadas de páginas web. En caso de citar, se necesita mencionar la fuente si fuera posible.</li>

<li>No deben promover la publicidad no deseada (SPAM) o pornografía infantil.</li>

<li>Al adjuntar y/o enlazar, deben estar libres de virus o software malintencionado.</li>

<li>Las publicaciones pagadas por una empresa deben ser señaladas al final de la página, sustentando el motivo.</li>
</ul>

<h3 id="responsabilidaddelosderechosdeautor">Responsabilidad de los derechos de autor</h3>

Las redacciones de solo texto son licenciadas por defecto bajo <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike</a> y <a href="https://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License</a>.

Para más detalles, adjuntamos <a href="{{ site.url }}/licencia">el archivo "Licencia"</a>. Puedes solicitar una licencia más permisiva que la primera. Por ejemplo, para distribuir sin copyleft recomendamos <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution</a>.

Otro contenido es propiedad intelectual de sus respectivos autores. Mayormente, etiquetamos "fair use" u "uso legítimo" con la intención de no lucrar hasta lo permitido por las leyes.

<h3 id="privacidad">Privacidad</h3>

Por el momento, esta página web usa cookies de Github para conocer el rendimiento. No se anexa información privada del usuario.

<h2 id="aplicacin">Aplicación</h2>

Ejemplos de abuso, acoso, falisificación de indentidades, u otro tipo de comportamiento inaceptable puede ser reportado al equipo del proyecto en <a href="{{ site.url }}/contact/">la página Contacto</a>. Todas las quejas serán revisadas e investigadas, generando un resultado apropiado a las circunstancias. El equipo del proyecto está obligado a mantener confidencialidad de la persona que reportó el incidente. Detalles específicos acerca de las políticas de aplicación pueden ser publicadas por separado.

Administradores que no sigan o que no hagan cumplir este Código de Conducta pueden ser eliminados de forma temporal o permanente del equipo administrador. De forma similar sucede con los usuarios en general.

En caso que el usuario promueva el terrorismo o pornografía infantil, amenace a los usuarios y tenga antecedentes, será reportado en privado a las autoridades correspondientes. Algunas sugerencias en <a href="https://www.genbeta.com/a-fondo/como-denunciar-a-la-policia-si-encuentro-contenido-pedofilo-en-internet">esta página</a>.

<h3 id="alcance">Alcance</h3>

Este código de conducta aplica tanto a espacios del proyecto como a espacios públicos donde un individuo esté en representación del proyecto o comunidad. Ejemplos de esto incluye el uso de la cuenta oficial de correo electrónico, publicaciones a través de las redes sociales oficiales, o presentaciones con personas designadas en eventos <em>online</em> u <em>offline</em>. La representación del proyecto puede ser clarificada explicitamente por los administradores del proyecto.

<h3 id="limitacioneslegales">Limitaciones legales</h3>

El contenido desarrollado por la comunidad no tiene una cláusula de garantía debido a la naturaleza gratuita de cooperar. Los administradores no se harán responsables frente a:

<ul>
<li>Las actividades maliciosas ajenas a la página del proyecto.</li>

<li>Pérdidas materiales o económicas realizadas por la comunidad sin ser probadas ni revisadas con antelación; o</li>

<li>Las acciones que podrían incitar a las actividades ilegales. En caso que ocurra, la comunidad no estará involucrada y cualquier pérdida del demandante correrá directamente al individuo responsables.</li>
</ul>

<h2 id="agradecimientos">Agradecimientos</h2>

Este Código de Conducta es una adaptación del <a href="http://contributor-covenant.org">Contributor Covenant</a>, versión 1.4, disponible en <a href="http://contributor-covenant.org/version/1/4/es/">http://contributor-covenant.org/version/1/4/es/</a>

Contiene extractos de <a href="https://ondahostil.wordpress.com/politica-de-troleo/">OndaHostil</a>, licenciado bajo CC-BY-SA 4.0.

Puedes crear una nueva versión de los términos, siempre y cuando nos atribuyas a Deepin en Español.
