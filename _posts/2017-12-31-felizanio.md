---
title: "Feliz año"
categories:
    - noticias
date: 2017-12-31
show_comments: true
---

A los colaboradores de Deepin en Español y usuarios en general, agradecemos por participar en esta comunidad. Especialmente, a los que fielmente nos siguieron, aportaron y dieron vida como proyecto.

Antes de terminar el año tenemos tres novedades y necesitamos de tu apoyo para crecer este proyecto:

<img src="{{ site.baseurl }}/images/posts/papel2018.jpg" alt="Sección de presentación de la Ayuda de Deepin">

## Nuevo repositorio
Carlos anunció el repositorio "deepines" recopilando programas adicionales (en forma de paquetes) como Telegram y Vidcutter.
El repositorio está alojado en los servidores de la Universidad de Paraná, Brasil, para una descarga más fluida. Un agradecimiento a Bruno Gonçalves (@bigbruno) por brindar ese servicio de alojamiento y la comunidad brasileña de Deepin.

* [Instala el repositorio en un clic](https://drive.google.com/open?id=13YwEiQwynWmWnrK0kIBQoIE-0FJvI5Ar)
* [Web del repositorio](http://biglinux.c3sl.ufpr.br/packages/deepines/main/)

Si tienes una aplicación de código abierto, [contacta con nosotros]({{ site.baseurl }}/redes/).

## Ayuda de Deepin
La Ayuda de Deepin sigue renovando, ahora incluiremos fondos de pantalla, más aplicaciones, preguntas frecuentes, comentarios (próximamente),

<img src="{{ site.baseurl }}/images/posts/presentacion.png" alt="Sección de presentación de la Ayuda de Deepin">

Tenemos 300 artículos y un buscador integrado para cualquier consulta frecuente.

## Donaciones
Está confirmadas las donaciones. Ahora puedes participar haciendo una aporte económico a Deepin Wuhan mediante [Paypal](https://www.paypal.me/deepindonate) (haz clic para donar). Siéntete libre a aportar cualquier cantidad de dinero y conocer los metas planeadas para 2018.

Fuente: [Foro de Deepin](https://bbs.deepin.org/forum.php?mod=viewthread&tid=150791)

## Un saludo final
Les deseamos un feliz 2018 con nuevas metas y objetivos.
