---
title: "Nuevo foro"
categories:
    - noticias
date: 2018-02-28
show_comments: true
---

Gracias por confiar en nuestro equipo. Desde febrero hemos anunciado la creación de un nuevo sitio web deepinenespanol.com, un nuevo foro y unos cuandos retoques.

## Un mensaje amistoso
Inicialmente la comunidad nació para ayudar a los usuarios a conocer Deepin desde 2015. Con el tiempo, la exigencia cada vez mayor, la comunidad se ha fortalecido con tus comentarios y apreciaciones.

No solo beneficiamos a los usuarios sino para el sistema operativo en general. Gracias a Kevin, que forma parte del equipo oficial en China, nos mantenemos al tanto de las actualizaciones y primicias de Deepin para el público en general. Si eres nuevo el mundo GNU/Linux, nosotros te daremos el primer paso.

<img src="{{ site.baseurl }}/images/posts/papel2018.jpg" alt="Imagen de la nota">

## Novedades en la organización
Además durante este 2018, nuestro equipo está creciendo. Damos la bienvenida a Eli Linares (nacido en Cuba) y Pablo (nacido en Perú). También está nuestro artista Akiba Ilusion (reside en Costa Rica). También está Carlos (colabora activamente), Joel (alias Jhalo, creador del grupo de Telegram), Alvaro Samudio (seguridad), Alejandro Camanera (conocido por el set Halo) y Diego (creador del sitio web original).

Con ese cambio, actualizaremos nuestra página de "Términos de uso de la comunidad", que está en fase de pruebas.

## El nuevo sitio web
* Por el momento, tenemos una serie de tutoriales y consejos realizados por los usuarios. Usamos como base Wordpress para facilitar el proceso de creación de artículos.
* Eres bienvenido a participar en Deepin Latin Code (grupo donde debatimos el repositorio y las aplicaciones elaboradas por el equipo), traducciones (junto a Transifex), wiki, diseños (Halo y Marea) entre otros.
* Una sección de preguntas frecuentes para responder las dudas a los miles de usuarios.
* Es posible que este sitio realice cambios inesperados debido a su creación en este año. El sitio web es "deepinenespañol.org" (o "deepinenespanol.com" para acceder sin recurrir al teclado en español).

## Ayuda de Deepin
La ayuda de Deepin seguirá creciendo y esta página es un respaldo para promover nuestra comunidad con otros usuarios. En la nueva website, varios colaboradores añdirán trucos y tutoriales para Deepin en la sección "wiki".

Muchas gracias por confiar en nosotros.

PD: Si preguntas sobre la nueva versión Deepin 15.5.1, espera para marzo. Incluirá mejoras para configurar el sistema (tipografía, controladores, etcétera), nueva tienda de aplicaciones, Crossover con soporte para Windows 7, aplicaciones actualizadas y correcciones de errores.
